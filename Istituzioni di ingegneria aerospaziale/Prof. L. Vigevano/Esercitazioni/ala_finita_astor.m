% cosa sono Octave e Matlab?
% "ambienti" di calcolo che interpretano dei "comandi" dati
% dall'utilizzatore con la sintassi tipica delle formule
%
% Octave introduzione disponibile su
% https://www.gnu.org/software/octave/doc/interpreter/Introduction.html
% Matlab: Tutorial disponibili sul sito www.mathworks.com alla voce Prodotti
%
% provvedono "autonomamente" alla gestione degli aspetti "informatici"
% lasciando l'utilizzatore concentrato sul problema 
%
% l'ambiente:
% il programma si lancia come tutti gli applicativi in ambiente grafico;
% Octave: normalmente compare una finestra unica con il "prompt" che contiene 
% il nome octave+sigla versione:+numerlo linea comando >
% il prompt > segnala che il programma � pronto per accettare comandi
%
% Matlab: normalmente compare una finestra divisa in 3 parti
% quella pi� importante � la "Command Window" riconoscibile perch� bianca
% e dalla presenza del "prompt" cio� >>
% >> segnala che il programma � pronto per accettare "comandi"
%
% i comandi possono essere scritti individualmente
% o memorizzati in un file detto "script" (in Matlab con estensione .m) 
% gli "script" e possono essere eseguiti semplicemente digitando il loro nome
% [non � possibile usare lo stesso nome per uno script e una variabile]
%
% useremo sempre "script", la modalit� interattiva prevede di scrivere una
% line per volta e dare "invio" per avviare l'interpretazione del comando
% [possibile fare "copia e incolla" da uno script alla linea di comando]
%
% Per acquisire familiarit� con l'ambiente di lavoro � utile:
% 1. lanciare il comando doc (Octave) o demo (Matlab) che illustrano
%    le potenzialit� del software attraverso esempi numerici e casi test;
% 2. fare riferimento all'uso dell'help, ad esempio help sqrt (calcolo radice)
% Molte informazioni possono essere reperite sul web

%
% Octave/Matlab sono codici che "interpretano" i "comandi", vale a dire li
% leggono e li elaborano uno dopo l'altro analizzando ogni stringa (insieme di
% caratteri che costituisce il "comando"); questo rende l'utilizzo del codice
% estremamente flessibile ma pi� lento, in quanto l'analisi del testo
% introdotto e la sua trasformazioni in "comandi" elementari richiede tempo
%
% comandi base di Octave/Matlab
% se una linea inizia con % viene utilizzata come un commento
% se su una linea si inserisce un % la rimanente parte � trattata come
% commento
%
% Definizione: 
% "variabile" coppia di un "nome" e un "valore"
% il "valore" � sempre associato al nome e per "vederlo" occorree fare
% riferimento al nome
%
% assegnazione di un valore ad una variabile
% nome=valore; 
% [ il ';' finale rende "muto" il comando, altrimenti viene visualizzato
% il risultato dell'assegnazione. Es.   S=101.35
%
% visualizzazione del valore di una variabile
% nome
% disp(nome)
% 


%DATI	

clear all
close all
S=101.35; %Superficie Alare
b=28.63; %Apertura Alare
profilo='NACA 2421'; %Profilo Alare
CR0f=0.15; %CR Fusoliera
CR0g=0.08; %CR Gondole
CR0i=0.08; %CR Impennaggi
Sf=7.17; %Superficie frontale fusoliera
Sg=1.05; %Superficie frontale gondola
Si=45.67; %Superificie impennaggi
lambda=8.088; %Allungamento alare
u=0.968; %Coefficiente di Oswald


% i dati sino ad ora utilizzati sono scalari
% 
% informazioni organizzate in colonne (o vettori), righe (vettori
% trasposti) o matrici (strutture almeno bidimensionali) si usano le
% parentesi quadre
% vettore_riga=[1 2 3 4];
% vettore_colonna=[1 2 3 4]';
% vettore_colonna=[1; 2; 3; 4]; [il ";" "manda a capo", quindi crea una nuova riga
%
% le matrici (o tabelle) verranno viste pi� avanti
% si anticipa che la regola generale � che nelle matrici TUTTE le righe
% e le colonne DEVONO avere un UGUALE NUMERO di elementi
% altrimenti si avr� la segnalazione:
% Octave:
% error: number of columns must match ( n != m)
% Matlab:
% All rows in the bracketed expression must have the same
% number of columns.

alfa=[-8 -4 0 4 8 12 16 20]';  % vettore incidenze (') indice di trasposizione
CP=[-0.6 -0.2 0.2 0.6 0.95 1.2 1.2 1.15]'; % tabella coeff portanza
CR=[0.01 0.008 0.0077 0.0085 0.012 0.02]'; % tabella coeff resistenza



% operazioni tra variabili
% [le operazioni tra variabili si eseguono "come si scrivono"
% es:   a=b+c;   significa che alla variabile a viene assegnato il valore
% somma di b+c
%
% valgono tutte le regole di priorit� dell'algebra
%
% le operazioni si applicano anche a vettori con le regole dell'Algebra
% esempio di operazione tra un vettore e variabili scalari
alfa_i=CP/pi/lambda/u*180/pi; % incidenza indotta espressa in gradi
                              % per ogni CP ho la variazione alfa_i corrispondente 
% operazioni tra vettori e matrici
% Matriciale: le dimensioni dei vettori devono essere coerenti e
% compatibili con l'opeazione
% somma: 
% va=vb+vc; vb e vc devono avere lo stesso numero di termini
% cio�: va(i)=vb(i)+vc(i); per i da 1 ad n_termini
% quindi: va avr� lo stesso numero di termini di vb e vc
% POSSO sommare due vettori dello stesso numero di termini
% NON POSSO sommare una riga e una colonna anche se dello stesso numero di termini
%
% prodotto: va=vb*vc;
%
% differenza tra vettori della stessa dimensione
alfa_e=alfa-alfa_i; % incidenza effettiva

% elevamento a potenza dei termini di un vettore
CR_i=CP.^2/pi/lambda/u;  % il '.' prima del carattere che identifica l'operazione
                         % fa applicare l'operazione ai singoli termini e
                         % non al vettore nel suo complesso
                         % alternativa: CR_i=power(CP,2)/pi/lambda/u; 
% ATTENZIONE: in un comando le variabili a destra dell'uguale devono essere
%             state definire PRIMA del comando stesso; in caso contrario si
%             avr� una segnalazione di errore
%             Per la variabile a sinistra dell'uguale avremo due casi:
%             variabile gi� creata: in questo caso il risultato
%             dell'operazione dovr� essere di dimensioni compatibili con
%             quella della variabile cui si vuole assegnare il risultato
%             (impossibile applicare le regole dell'eguaglianza che
%             imporrebbero di copiare termine a termine)
%             variabile inesistente: la variabile viene creata con
%             dimensioni coerenti con quelle del risultato dell'espressione
% quindi:
% a=b+c;   "b" e "c" devono esistere ed avere un valore per poter calcolare 
%          l'espressione "b+c"
%          la variabile "a" viene creata con le dimensioni del risultato
%          "b+c" (in quanto l'espressione vale anche nel caso di "b" e "c"
%          matrici

% calcolo DeltaCR di forma per velivolo completo
% CRfus*Sfus+2*CRgondole*Sgondole+CRimpennaggi*Simpennaggi)/Salare
DCR=(CR0f*Sf+2*CR0g*Sg+CR0i*Si)/S;

% calcolo CRvelivolo_parziale
% n=length(v) fornisce la dimensione del vettore v e lo memorizza in n
% [nr,nc]=size(m) fornisce le dimensioni della matrice m e le memorizza
% nelle variabili nr (numero di righe) e nc (numero di colonne)
% il risultato pu� essere impiegato direttamente in una formula

CR_par=CR+CR_i(1:length(CR)); % usa solo la porzione di CR_i che trova corrispondenza
                              % in CR che � un vettore pi� corto
CR_tot=CR_par+DCR;    % calcolo CR totale



% tipologia di informazioni:
% si notano due tipologie: numeri reali e una "stringa alfanumerica"
% Octave/Matlab tratta nello stesso modo numeri reali ed interi 
% (se non gli si dice altro)
% il comando "whos" permette di visualizzate l'elenco delle variabili
% presenti e la loro tipologia: nome, dimensioni, spazio (in Bytes) tipo
% per uno scalare reale si legger�:
% Name       Size             Bytes     Class
% nome       1x1                8       double
% per un vettore riga di 8 posizioni si legger�:
% Name       Size             Bytes     Class
% nome       1x8                64      double
% per una stringa di 9 caratteri si legger�:
% nome       1x9                18      char 

% visualizzazione risultati
% omettendo ";" alla fine di un comando si ottiene l'echo (visualizzazione)
% del risultato dell'operazione
% provate a scrivere:     b=3;
%                         c=4;
%                         b+c
%                         a=b+c  o   a=b+c;
% il formato di visualizzazione � reale con 4 cifre decimali (5
% significative)
% tale formato pu� essere modificato (a 15 cifre significative) con il comando
% "formal long" 
% con il comando "format short" si torna a quello standard


% tracciatura grafici
% comandi base
% figure(n)  [crea la figura numero n]
% nella barra di comando compaiono delle icone (sotto un altro simolo per
% Octave, sotto quello principale per Matlab (insieme all'editor degli script)
%
% set(figure(n),'name','Titolo figura') [al numero associa il titolo]   
% plot(x,y)   [crea un diagramma xy con assi automatici]
% plot(x1,y2,x2,y2,...)
figure(1)  % crea la figura n.1
plot(alfa,CP,alfa_e,CP) % visualizza le curve cp-alfa e cp-alfavel

figure(2)  % figura con curva polare profilo
plot(CP(1:length(CR)),CR_tot)

figure(3) % figure con curve polari profilo e velivolo
plot(CP(1:length(CR)),CR,'^--',CP(1:length(CR)),CR_par,'s-.',CP(1:length(CR)),CR_tot,'o-')

% i grafici costituiscono finestre indipendenti che possono essere chiuse/ 
% stampate/copiate (es importate in un documento)
%
% confrontate la leggibilit� delle figure 1,2 e 3 con 11,12 e 13
%
% i primi tre grafici sono inintelleggibili
%
% le regole della documentazione tecnica IMPONGONO di renderli chiari indicando:
% per ogni asse denominazione e unit� di misura 
% identificazione delle curve
% [se significativo un titolo del grafico]
%
% vediamo come arricchire i grafici

% [i punti della tabella vengono collegati da una linea]
% [ se si vogliono solo i punti si usa comando: plot(x,y,'s')]
% [per simboli possibili digitare 'help plot']
% [per cambiare lo stile della linea si usa comando: plot(x,y,'--')]
% - linea continua,  -. punto-linea, --tratteggio, : curva a puntini
% [le curve sono colorate automaticamente in funzione del numero d'ordine]
% [� possibile assegnare dei colori con l'opportuno codice]
% [informazioni generali per ogni curva: plot(x,y,opzioni) 
%  nelle opzioni si combinano 'tipo linea' 'simbolo' 'colore'

figure(11)  % crea la figura n.1
set(figure(11),'name','CurveCp/alfa')   % assegna un nome alla figura 
% la stringa dichiarata compare nel titolo della finestra insieme al numero
% es  Figure 1:CurveCp/alfa
plot(alfa,CP,'s-',alfa_e,CP,'o-.') % visualizza le curve cp-alfa e cp-alfavel
% la prima � in linea continua '-' e su ogni punto c'� un "marker" quadrato 's' (square)
% la seconda � a tratto-punto '-.' e su ogni punto c'� un cerchio 'o' 
% 1) si pu� omettere il simbolo del "marker" e la curva collegher� i punti con
% tratti rettilinei
% plot(alfa,CP,'-',alfa_e,CP,'-.') 
% 2) si pu� omettere la definizione della linea e la curva sar� tracciata
% per punti
% plot(alfa,CP,'s',alfa_e,CP,'o') 


legend('Cp/alfa Velivolo','Cp/alfa Profilo',4) % mette i nomi delle curve sul grafico
% il 4 identifica la posizione: 1 e 2 sono rispettivamente angoli
% superiori destro e sinistro, 3 e 4 angoli inferiori, sinistro e destro

xlabel('Alfa [deg]') % identifica l'asse x
ylabel('Cp') % identifica l'asse y
title('Curva Cp/alfa')  % titolo del grafico (appare sul grafico)
grid on   % crea la griglia nel grafico

figure(12)
set(figure(12),'name','Polare profilo')   % assegna un nome alla figura 
plot(CP(1:length(CR)),CR_tot)
xlabel('Cp')
ylabel('Cr')
titolo=['Polare profilo: ' profilo ]; % unione di due stringhe (concatenazione)
title(titolo) % 
grid on   

figure(13)
set(figure(13),'name','Polari')   % assegna un nome alla figura 
plot(CP(1:length(CR)),CR,'^--',...    % tre punti affiancati permettono di
    CP(1:length(CR)),CR_par,'s-.',... % spezzare un comando troppo lungo
    CP(1:length(CR)),CR_tot,'o-')     % su pi� righe
legend('Profilo','Velivolo parziale','Velivolo completo',2)
xlabel('Cp')
ylabel('Cr')
title(['Polari'])
grid on   




